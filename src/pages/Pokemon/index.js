import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from './action';
import Main from './Main';

function mapStateToProps(state) {
  const { pokemonList, totalData, pokemonDetail } = state.pokemon;
  return {
    totalData,
    pokemonList,
    pokemonDetail,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
    dispatch
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Main);
