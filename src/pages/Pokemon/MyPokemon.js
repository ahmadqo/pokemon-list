/** @jsxImportSource @emotion/react */
import { css } from "@emotion/react";
import { useEffect, useState } from "react";
import { IMAGES } from "../../constants";
import { getMyPokemon, setMyPokemon } from "../../utils/common";
import "./style.scss";

const MyPokemon = ({ handleDetail, setOpenMyPoke }) => {
  const card = css`
    border: 1px solid #eee;
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-bottom: 15px;
    background-color: #47d1af;
    border-radius: 12px;
    padding: 5px 15px;
    color: #fff;
    text-transform: uppercase;
    font-weight: 600;
    cursor: pointer;
  `;

  const imgPoke = css`
    background-color: #63d7bb;
    border-radius: 50%;
  `;

  const sectionTop = css`
    position: fixed;
    top: 0;
    z-index: 1;
    background: #fff;
    width: 100%;
    left: 0;
    display: flex;
    align-items: center;
    padding: 5px 15px;
  `;

  const btnX = css`
    position: absolute;
    right: 17px;
    background: red;
    color: white;
    width: 24px;
    height: 24px;
    text-align: center;
    border-radius: 30px;
    cursor: pointer;
    margin-top: -8px;
  `;

  const [list, setList] = useState([]);

  useEffect(() => {
    if (getMyPokemon() && getMyPokemon().length > 0) {
      setList(getMyPokemon());
    }
  }, []);

  const handleRemove = (e, name) => {
    e.preventDefault();
    const arrMyPokemon = [...list];

    arrMyPokemon.splice(
      arrMyPokemon.findIndex((item) => item.nickName === name),
      1
    );
    setMyPokemon(arrMyPokemon);
    setList(arrMyPokemon);
  };

  const renderCardMyPokemon = () => {
    return list.map((item, i) => (
      <div key={i} className="col-lg-4 col-md-12">
        <div css={btnX} onClick={(e) => handleRemove(e, item.nickName)}>
          &#10005;
        </div>
        <div
          css={card}
          onClick={(e) => {
            handleDetail(e, item.name);
            setOpenMyPoke(false);
          }}
        >
          <div>
            <div css={{ marginBottom: "15px", textTransform: "capitalize" }}>
              {item.nickName}
            </div>
            <div>{item.name}</div>
          </div>
          <div css={imgPoke}>
            <img src={item.image} alt={`image_${item.name}`} />
          </div>
        </div>
      </div>
    ));
  };

  return (
    <div className="container-fluid">
      <section css={sectionTop}>
        <div
          onClick={() => setOpenMyPoke(false)}
          css={{
            cursor: "pointer",
            width: "fit-content",
            display: "flex",
            alignItems: "center",
          }}
        >
          <img src={IMAGES.arrowBack} alt="back" css={{ width: "34px" }} />
          <span
            css={{ marginLeft: "15px", fontWeight: "600", fontSize: "18px" }}
          >
            My Pokemon
          </span>
        </div>
      </section>
      <div className="row" style={{ padding: "15px", marginTop: "42px" }}>
        {list.length > 0 && renderCardMyPokemon()}
      </div>
    </div>
  );
};

export default MyPokemon;
