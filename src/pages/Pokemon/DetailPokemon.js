/** @jsxImportSource @emotion/react */
import { css } from "@emotion/react";
import { useEffect, useState } from "react";
import { IMAGES } from "../../constants";
import { setMyPokemon, getMyPokemon } from "../../utils/common";
import { getTypeStyle } from "../../utils/common";

const DetailPokemon = ({ pokemonDetail, pokemonList, setOpenDetail }) => {
  const [getPokemon, setGetPokemon] = useState(false);
  const [isCaughtPokemon, setCaughtPokemon] = useState(false);
  const [messageGet, setMessageGet] = useState("");
  const [pokemonName, setPokemonName] = useState("");
  const [isErrorNickName, setIsErrorNickname] = useState(false);

  const [listMoves, setMoves] = useState([]);
  const [listTypes, setTypes] = useState([]);
  const [listAbilities, setAbilities] = useState([]);
  const [listStats, setStats] = useState([]);

  const [tabOpen, setTabOpen] = useState(1);

  useEffect(() => {
    if (pokemonDetail) {
      const arrMoves = [];
      const arrTypes = [];
      const arrAbilities = [];
      const arrStats = [];

      const { moves, types, abilities, stats } = pokemonDetail;
      if (moves && moves.length > 0) {
        moves.map((item) => arrMoves.push(item.move.name));
      }

      if (types && types.length > 0) {
        types.map((item) => arrTypes.push(item.type.name));
      }

      if (abilities && abilities.length > 0) {
        abilities.map((item) => arrAbilities.push(item.ability.name));
      }

      if (stats && stats.length > 0) {
        stats.forEach((item) => {
          arrStats.push({
            name: item.stat.name,
            base: item.base_stat,
          });
        });
      }

      setMoves(arrMoves);
      setTypes(arrTypes);
      setAbilities(arrAbilities);
      setStats(arrStats);
    }
  }, [pokemonDetail]);

  const _onBack = () => {
    setOpenDetail(false);
    setGetPokemon(false);
    setCaughtPokemon(false);
    setMessageGet("");
    setPokemonName("");
  };

  const _onClose = () => {
    setGetPokemon(false);
    setCaughtPokemon(false);
    setMessageGet("");
    setIsErrorNickname(false);
  };

  const getRandomInt = (max) => {
    return Math.floor(Math.random() * Math.floor(max));
  };

  const _onCatchPokemon = () => {
    if (!getPokemon) {
      setGetPokemon(true);
      if (getRandomInt(2) === 1) {
        setCaughtPokemon(true);
        setMessageGet("Hooray, You catch a Pokemon");
      } else {
        setMessageGet("Pokemon failed to catch");
      }
    }
  };

  useEffect(() => {
    if (getPokemon) {
      setTimeout(() => {
        setGetPokemon(false);
        setMessageGet("");
      }, 1000);
    }
  }, [getPokemon]);

  const _onChangePokemonName = (e) => {
    const { value } = e.target;
    setPokemonName(value);
    setIsErrorNickname(false);
  };

  const _onSavePokemon = () => {
    console.log(pokemonName);
    if (pokemonName.length > 0) {
      const filter = pokemonList.filter(
        (poke) => poke.name === pokemonDetail.name
      );
      if (filter && filter[0]) {
        const Obj = {
          ...filter[0],
          nickName: pokemonName,
        };
        const myPokemon = getMyPokemon();
        if (myPokemon === null) {
          setMyPokemon([Obj]);
          _onBack();
        } else {
          const arr = [...myPokemon];
          const filterNickName = arr.filter(
            (myPk) => myPk.nickName === pokemonName
          );
          if (filterNickName && filterNickName[0]) {
            setIsErrorNickname(true);
          } else {
            arr.push(Obj);
            setMyPokemon(arr);
            _onBack();
          }
        }
      }
    }
  };

  const container = css`
    background-color: #fed86d;
    padding-top: 15px;
    height: 100%;
  `;
  const back = css`
    cursor: pointer;
    width: "fit-content";
  `;
  const pokeName = css`
    color: #fff;
    font-size: 32px;
    font-weight: 600;
    text-transform: capitalize;
    margin-top: 20px;
    margin-bottom: 70px;
  `;
  const rowDesc = css`
    background: white;
    border-top-left-radius: 40px;
    border-top-right-radius: 40px;
    padding: 24px;
  `;
  const btnTab = css`
    display: flex;
    justify-content: space-between;
    padding: 10px 20px;
    padding-top: 30px;
    border-bottom: 1px solid #c8c8c8;
    & div {
      cursor: pointer;
      font-size: 16px;
      font-weight: 600;
      color: #c8c8c8;
    }
  `;
  const imgPoke = css`
    position: absolute;
    right: 24px;
    top: 8%;
    width: 170px;
    height: 170px;
    display: flex;
    & img {
      width: 100%;
    }
  `;

  const tabActive = css`
    color: #fed86d !important;
  `;
  const labelTitle = css`
    font-size: 18px;
    font-weight: 600;
    margin-bottom: 20px;
  `;
  const labelBox = css`
    padding: 5px 15px;
    margin: 5px 10px 5px 0;
    border-radius: 8px;
    background-color: #fed86d;
  `;
  const alert = css`
    font-size: 20px;
    font-weight: 600;
    position: fixed;
    left: 50%;
    top: 10%;
    transform: translate(-49%, -62%);
    width: fit-content;
    padding: 10px 30px;
    border-radius: 6px;
    background-color: #000;
    color: black;
    opacity: 0.6;
    min-height: 40px;
  `;

  const danger = css`
    background-color: rgba(255, 0, 0, 0.8) !important;
  `;

  const btnCatch = css`
    background-color: #89cdb1;
    border-radius: 100px;
    font-size: 16px;
    font-weight: 600;
    height: 100px;
    width: 100px;
    display: flex;
    align-items: center;
    text-align: center;
    opacity: 0.6;
    bottom: 30px;
    cursor: pointer;
    position: fixed;
    left: 50%;
    bottom: -5%;
    transform: translate(-49%, -62%);
  `;

  const modal = css`
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-49%, -62%);
    background: #fff;
    width: 90%;
    padding: 16px;
    border: 1px solid #c8c8c8;
    border-radius: 20px;
  `;

  const btnClose = css`
    position: absolute;
    right: 12px;
    font-size: 32px;
    top: 0;
    cursor: pointer;
  `;

  const btnSave = css`
    background-color: #3f51b5;
    color: white;
    font-weight: 600;
    font-size: 18px;
    border-radius: 30px;
    padding: 10px 20px;
    margin-top: 32px;
    text-align: center;
  `;

  return (
    <div className="container-fluid" css={container}>
      <div onClick={_onBack} css={back}>
        <img
          src={IMAGES.arrowBack}
          alt="back"
          css={{ width: "34px", filter: "brightness(0) invert(1)" }}
        />
      </div>

      {getPokemon && !isCaughtPokemon && (
        <div css={[alert, !isCaughtPokemon && danger]}>{messageGet}</div>
      )}

      <div css={pokeName}>{pokemonDetail.name}</div>

      {pokemonDetail.sprites && (
        <div css={imgPoke}>
          <img
            className="d-block"
            src={pokemonDetail.sprites.front_default}
            alt="depan"
          />
        </div>
      )}

      <div className="row" css={rowDesc}>
        <div css={btnTab}>
          <div css={tabOpen === 1 && tabActive} onClick={() => setTabOpen(1)}>
            Types
          </div>
          <div css={tabOpen === 2 && tabActive} onClick={() => setTabOpen(2)}>
            Stats
          </div>
          <div css={tabOpen === 3 && tabActive} onClick={() => setTabOpen(3)}>
            Moves
          </div>
          <div css={tabOpen === 4 && tabActive} onClick={() => setTabOpen(4)}>
            Sprites
          </div>
        </div>

        {tabOpen === 1 && (
          <div css={{ marginTop: "20px" }}>
            <div css={labelTitle}>Types</div>
            <div
              css={{
                display: "flex",
                flexWrap: "wrap",
                marginBottom: "20px",
              }}
            >
              {listTypes.length > 0 &&
                listTypes.sort().map((item, i) => (
                  <div
                    key={i}
                    css={{
                      color: "#FFF",
                      backgroundColor: getTypeStyle(item),
                      padding: "5px 15px",
                      margin: "5px 10px 5px 0",
                      borderRadius: "8px",
                    }}
                  >
                    {item}
                  </div>
                ))}
            </div>

            <div css={labelTitle}>Abilities</div>
            <div
              css={{
                display: "flex",
                flexWrap: "wrap",
                marginBottom: "20px",
              }}
            >
              {listAbilities.length > 0 &&
                listAbilities.sort().map((item, i) => (
                  <span key={i} css={labelBox}>
                    {item}
                  </span>
                ))}
            </div>
          </div>
        )}

        {tabOpen === 2 && (
          <div css={{ marginTop: "20px" }}>
            <div css={labelTitle}>Base Stats</div>
            <div>
              {listStats.length > 0 &&
                listStats.sort().map((item, i) => (
                  <div
                    key={i}
                    css={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "space-between",
                    }}
                  >
                    <div
                      css={{ textTransform: "capitalize", fontWeight: "600" }}
                    >
                      {item.name}
                    </div>

                    <div>{item.base}</div>
                  </div>
                ))}
            </div>
          </div>
        )}

        {tabOpen === 3 && (
          <div css={{ marginTop: "20px" }}>
            <div css={{ display: "flex", flexWrap: "wrap" }}>
              {listMoves.length > 0 &&
                listMoves.sort().map((item, i) => (
                  <span key={i} css={labelBox}>
                    {item}
                  </span>
                ))}
            </div>
          </div>
        )}

        {tabOpen === 4 && (
          <div css={{ marginTop: "20px" }}>
            {pokemonDetail.sprites && (
              <div css={{ display: "flex", flexWrap: "wrap" }}>
                <img
                  className="d-block"
                  src={pokemonDetail.sprites.front_default}
                  alt="depan"
                  css={{ width: "170px" }}
                />
                <img
                  className="d-block"
                  src={pokemonDetail.sprites.back_default}
                  alt="belakang"
                  css={{ width: "170px" }}
                />
              </div>
            )}
          </div>
        )}

        <div css={btnCatch} onClick={() => _onCatchPokemon()}>
          Catch Pokemon
        </div>
      </div>

      {isCaughtPokemon && (
        <div css={modal}>
          <div
            css={{
              color: "#3F51B5",
              fontsize: "28px",
              textAlign: "center",
              fontWeight: "600",
            }}
          >
            {messageGet}
          </div>
          <div css={btnClose} onClick={() => _onClose()}>
            &times;
          </div>
          <div css={{ marginTop: "32px", fontWeight: "600", fontsize: "19px" }}>
            Give Your Pokemon a Nickname
          </div>
          <input
            type="text"
            className={`form-control form-custom ${isErrorNickName && "error"}`}
            id="txtPokemonName"
            name="pokemonName"
            onChange={_onChangePokemonName}
            css={{ height: "48px" }}
          />
          {isErrorNickName && (
            <div
              css={{
                color: "red",
                padding: "0",
                fontsize: "18px",
                fontWeight: 600,
              }}
            >
              Nickname already userd!
            </div>
          )}
          <div css={btnSave} onClick={_onSavePokemon}>
            Save Pokemon
          </div>
        </div>
      )}
    </div>
  );
};

export default DetailPokemon;
