/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import { withRouter } from "react-router-dom";
import ListPokemon from "./ListPokemon";
import DetailPokemon from "./DetailPokemon";
import MyPokemon from "./MyPokemon";

const Pokemon = ({ actions, pokemonList, totalData, pokemonDetail }) => {
  const limit = 30;
  const [offset, setOffset] = useState(0);
  const [openMyPoke, setOpenMyPoke] = useState(false);
  const [openDetail, setOpenDetail] = useState(false);

  const { fetchListPokemon, fetchDetailPokemon } = actions;

  useEffect(() => {
    if (!openDetail && !openMyPoke) {
      fetchListPokemon(limit, offset);
    }
  }, [limit, offset]);

  const handleDetail = (e, item) => {
    e.preventDefault();
    if (item) fetchDetailPokemon(item);
    setOpenDetail(true);
  };

  const handleNext = () => {
    setOffset(offset + limit + 1);
  };

  const handlePrev = () => {
    if (offset > 0) setOffset(offset - (limit + 1));
  };

  return (
    <>
      {!openDetail && !openMyPoke && (
        <ListPokemon
          pokemonList={pokemonList}
          handleDetail={handleDetail}
          offset={offset}
          handlePrev={handlePrev}
          totalData={totalData}
          limit={limit}
          handleNext={handleNext}
          setOpenMyPoke={setOpenMyPoke}
        />
      )}
      {openDetail && pokemonDetail !== null && !openMyPoke && (
        <DetailPokemon
          pokemonList={pokemonList}
          pokemonDetail={pokemonDetail}
          setOpenDetail={setOpenDetail}
        />
      )}

      {openMyPoke && (
        <MyPokemon handleDetail={handleDetail} setOpenMyPoke={setOpenMyPoke} />
      )}
    </>
  );
};
export default withRouter(Pokemon);
