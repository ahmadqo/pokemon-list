import { ACTIONS } from "../../constants";
import fetch from "../../utils/fetch";

export function setListPokemon(data, total) {
  return {
    type: ACTIONS.SET_POKEMON_LIST,
    totalData: total,
    pokemonList: data,
  };
}

export function fetchListPokemon(limit, offset) {
  const gqlQuery = `query pokemons($limit: Int, $offset: Int) {
    pokemons(limit: $limit, offset: $offset) {
      count
      next
      previous
      status
      message
      results {
        url
        name
        image
      }
    }
  }`;

  const gqlVariables = {
    limit: limit,
    offset: offset,
  };

  return (dispatch) => {
    const options = {
      method: "POST",
      url: "https://graphql-pokeapi.graphcdn.app/",
      credentials: "omit",
      headers: { "Content-Type": "application/json" },
      data: JSON.stringify({
        query: gqlQuery,
        variables: gqlVariables,
      }),
    };
    fetch(options)
      .then((res) => {
        if (res?.data?.pokemons) {
          const { results, count } = res.data.pokemons;
          dispatch(setListPokemon(results, count));
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };
}

export function setDetailPokemon(data) {
  return {
    type: ACTIONS.SET_POKEMON_DETAIL,
    pokemonDetail: data,
  };
}

export function fetchDetailPokemon(name) {
  const gqlQuery = `query pokemon($name: String!) {
    pokemon(name: $name) {
      id
      name
      abilities {
        ability {
          name
        }
      }
      moves {
        move {
          name
        }
      }
      types {
        type {
          name
        }
      }
      sprites {
        back_default
        front_default
      }
      stats {
        base_stat
        stat{
          name
        }
      }
      message
      status
    }
  }`;

  const gqlVariables = {
    name: name,
  };

  return (dispatch) => {
    dispatch(setDetailPokemon(null));

    const options = {
      method: "POST",
      url: "https://graphql-pokeapi.graphcdn.app/",
      credentials: "omit",
      headers: { "Content-Type": "application/json" },
      data: JSON.stringify({
        query: gqlQuery,
        variables: gqlVariables,
      }),
    };
    fetch(options)
      .then((res) => {
        if (res?.data?.pokemon) {
          dispatch(setDetailPokemon(res.data.pokemon));
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };
}
