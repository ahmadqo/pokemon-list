/* eslint-disable import/no-anonymous-default-export */
import React from 'react';

const fallback = <div>Loading...</div>;
const Suspensed = Element =>
  function suspense(props) {
    return (
      <React.Suspense fallback={fallback}>
        <Element {...props} />
      </React.Suspense>
    );
  };

export default {
  Pokemon: Suspensed(React.lazy(() => import('./Pokemon'))),
};