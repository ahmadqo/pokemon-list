import { ROUTES } from "../constants";
import pages from "../pages";

const menu = [
  {
    url: ROUTES.POKEMON(),
    page: pages.Pokemon,
  },
];

export default menu;
