import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import menus from './routes/menus';
import { MenuContext } from './contexts';

function App() {
  return (
    <MenuContext.Provider>
      <Switch>
        {menus.map((item, key) => (
          <Route
            key={key}
            exact
            path={item.url}
            component={item.page}
          />
        ))}
        <Redirect from="*" to="/" />
      </Switch>
    </MenuContext.Provider>
  );
}

export default App;
