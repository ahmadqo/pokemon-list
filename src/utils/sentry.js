import * as Sentry from '@sentry/browser';

export function initSentry() {
  Sentry.init({
    dsn: process.env.REACT_APP_SENTRY_ID,
  });
}
