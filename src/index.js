import React from "react";
import { render } from "react-dom";
import configureStore, { history } from "./store/configureStore";
import Root from "./Root";
import "bootstrap/dist/css/bootstrap.min.css";
import "./style.scss";

const store = configureStore();

render(
  <Root history={history} store={store} />,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
