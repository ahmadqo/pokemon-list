import { ACTIONS } from '../constants';

const initialState = {
  pokemonList: [],
  totalData: 0,
  pokemonDetail: null,
  myPokemonList: [],
};

export default function reducer(state = initialState, action) {
  const {
    SET_POKEMON_LIST,
    SET_POKEMON_DETAIL,
    SET_MY_POKEMON
  } = ACTIONS;

  const {
    type,
    pokemonList,
    pokemonDetail,
    myPokemonList,
    totalData
  } = action;

  switch (type) {
    case SET_POKEMON_LIST:
      return {
        ...state,
        totalData,
        pokemonList
      }
    case SET_POKEMON_DETAIL:
      return {
        ...state,
        pokemonDetail
      }
    case SET_MY_POKEMON:
      return {
        ...state,
        myPokemonList
      }
    default:
      return state;
  }
}