import { connectRouter } from 'connected-react-router';
import { combineReducers } from 'redux';
import pokemon from './pokemon';


const rootReducer = history =>
  combineReducers({
    router: connectRouter(history),
    pokemon
  });

export default rootReducer;
