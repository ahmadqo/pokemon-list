import routes from './routes';
import actions from './actions';
import images from './images';

export const ROUTES = routes;
export const ACTIONS = actions;
export const IMAGES = images;

export const MY_POKEMON_STORAGE = 'my_pokemon_storage';