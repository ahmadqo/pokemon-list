const actions = {
  SET_POKEMON_LIST: 'SET_POKEMON_LIST',
  SET_POKEMON_DETAIL: 'SET_POKEMON_DETAIL',
  SET_MY_POKEMON: 'SET_MY_POKEMON',
}

export default actions;